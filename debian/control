Source: ruby-merb-core
Section: ruby
Priority: optional
Maintainer: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Tollef Fog Heen <tfheen@debian.org>
Build-Depends: debhelper (>= 9~),
               gem2deb
Standards-Version: 3.9.7
Vcs-Git: https://anonscm.debian.org/git/pkg-ruby-extras/ruby-merb-core.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-ruby-extras/ruby-merb-core.git
Homepage: https://github.com/wycats/merb
XS-Ruby-Versions: all

Package: ruby-merb-core
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Replaces: merb-core (<< 1.1.3-1~)
Breaks: merb-core (<< 1.1.3-1~)
Provides: merb-core
Depends: bundler,
         rake,
         ruby | ruby-interpreter,
         ruby-erubis (>= 2.6.2),
         ruby-extlib (>= 0.9.13),
         ruby-mime-types (>= 1.16),
         ruby-rack,
         ${misc:Depends},
         ${shlibs:Depends}
Description: Core libraries for the Merb MVC framework
 Ruby-based MVC framework that is agnostic to ORM, JavaScript library,
 and template languages. Merb is plugin-based, so the core is small and
 well organized.

Package: merb-core
Section: oldlibs
Priority: extra
Architecture: all
Depends: ruby-merb-core,
         ${misc:Depends}
Description: Transitional package for ruby-merb-core
 This is a transitional package to ease upgrades to the ruby-merb-core
 package. It can safely be removed.
